# Microsoft Azure for Java Developers

In this repository you'll find the slidedeck and all resources used in the demo's given during the session [Microsoft Azure for Java Developers](https://tweakers.net/partners/devsummit/1500/daviddehoop/) at the Tweakers Developers Summit 2021. The contents include:

- Node.js application based on template generated with [Express](http://expressjs.com/).
- Sping Boot application based on template generated [Spring Initializr](https://start.spring.io/).
- ARM Templates for all the Azure resources used.
- Slidedeck of the presentation.

For any extra help or resources for trying out these demo's yourself have a look at [Microsoft's Quickstarts](https://docs.microsoft.com/en-us/azure/app-service/quickstart-nodejs?pivots=platform-linux) for App Service with either Node.js or Java (Spring Boot). If you still have questions don't hesitate to drop me a message.

## Node.js Project

The Node.js project was generated with Express with dependencies already injected. To resolve the latest dependencies with `npm` run the following command.

```
npm install
```

With Visual Studio Code and the [Azure Tools](https://marketplace.visualstudio.com/items?itemName=ms-vscode.vscode-node-azure-pack) extension installed you can deploy to the App Service by right clicking on the folder and select `Deploy to Web App...`.

## Spring Boot Project

The Spring Boot project was built with Maven and is based on Java 8. Make sure that you have the latest Java 8 JDK installed and [installed and configured](https://maven.apache.org/install.html) Maven.

To add the Azure Web App configuration as a plugin to your `pom.xml` in the Maven project run the following:

```
mvn com.microsoft.azure:azure-webapp-maven-plugin:1.14.0:config
```

To then deploy the Spring Boot application to the App Service run:

mvn clean
mvn install

```
mvn package azure-webapp:deploy
```
