----------------------Java MVN Commands----------------------
1. Add Maven App Service Plugin.
mvn com.microsoft.azure:azure-webapp-maven-plugin:1.14.0:config

2. Deploy Web App
mvn package azure-webapp:deploy

----------------------Node.js npm Commands----------------------
let appInsights = require('applicationinsights');
appInsights.setup("690b7e87-3170-4e6e-b880-394278451849").start();

1. Retrieve dependencies
npm install

2. Right click web app deploy.

  <plugin>
        <groupId>com.microsoft.azure</groupId>
        <artifactId>azure-webapp-maven-plugin</artifactId>
        <version>1.14.0</version>
        <configuration>
          <schemaVersion>v2</schemaVersion>
          <subscriptionId>73932b33-4f52-4dc2-bed4-a9aa97a0084b</subscriptionId>
          <resourceGroup>azure-workshop-david-de-hoop</resourceGroup>
          <appName>devsummitspringboot</appName>
          <pricingTier>B1</pricingTier>
          <region>westeurope</region>
          <appServicePlanName>devsummit-plan</appServicePlanName>
          <appServicePlanResourceGroup>azure-workshop-david-de-hoop</appServicePlanResourceGroup>
          <runtime>
            <os>Linux</os>
            <javaVersion>Java 8</javaVersion>
            <webContainer>Java SE</webContainer>
          </runtime>
          <deployment>
            <resources>
              <resource>
                <directory>${project.basedir}/target</directory>
                <includes>
                  <include>*.jar</include>
                </includes>
              </resource>
            </resources>
          </deployment>
        </configuration>
      </plugin>