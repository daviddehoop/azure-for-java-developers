# Microsoft Azure for Java Developers

In this repository you'll find the slidedeck and all resources used in the demo's given during the session [Microsoft Azure for Java Developers](https://devconf.nl/programma/2022/9) at the DevConf 2022. The contents include:

- Sping Boot application based on template generated [Spring Initializr](https://start.spring.io/).
- ARM Templates for all the Azure resources used.
- Slidedeck of the presentation.

## Spring Boot Project

The Spring Boot project was built with Maven and is based on Java 11. Make sure that you have the latest Java 11 JDK installed and [installed and configured](https://maven.apache.org/install.html) Maven.

To add the Azure Web App configuration as a plugin to your `pom.xml` in the Maven project run the following:

```
mvn com.microsoft.azure:azure-webapp-maven-plugin:1.14.0:config
```

To then deploy the Spring Boot application to the App Service run:

```
mvn package azure-webapp:deploy
```

### Application Properties

All the Azure configuration properties can be found in the `application.properties` file. This file contains the settings for pointing at the correct Key Vault and also contains the instrumentation key to link to your Application Insights Resource. The file can be found in the src/main/resources folder.

```
azure.keyvault.enabled=true
azure.keyvault.uri=<YOUR_KEY_VAULT_URI_HERE>
azure.application-insights.instrumentation-key=<YOUR_INSTRUMENTATION_KEY_HERE>
```