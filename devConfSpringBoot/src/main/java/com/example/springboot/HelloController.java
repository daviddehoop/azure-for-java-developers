package com.example.springboot;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import com.microsoft.applicationinsights.TelemetryClient;
import com.microsoft.applicationinsights.telemetry.MetricTelemetry;

@RestController
public class HelloController {

    @Value("${notSoSecret}")
    public String notSoSecret;

    @Autowired
    TelemetryClient telemetryClient;

	@RequestMapping("/")
	public String index() {
        telemetryClient.trackEvent("URI /index is triggered");
        return "AzConf 2022 Is Awesome!";
	}

	@RequestMapping("/secret")
	public String secret() {
        return "This is an Azure Key Vault Secret: " + notSoSecret;
	}

	@RequestMapping("/fibonacci")
	public String computeFibonacci() {    
        // Measure compute time.
        long startTime = System.nanoTime();
        String fibonacci = "Fibonacci sequence: " + fib(9);
        long endTime = System.nanoTime();

        // Track custom metric in Application Insights.
        MetricTelemetry benchmark = new MetricTelemetry();
        benchmark.setName("Fibonacci compute time");
        benchmark.setValue(endTime - startTime);
        telemetryClient.trackMetric(benchmark);

        return fibonacci;

    }

	// Fibonacci Series computation
	public static int fib(int n)
    {
        /* Declare an array to store Fibonacci numbers. */
        int f[] = new int[n + 1];
        int i;
  
        /* 0th and 1st number of the series are 0 and 1*/
        f[0] = 0;
  
        if (n > 0) {
            f[1] = 1;
  
            for (i = 2; i <= n; i++) {
                /* Add the previous 2 numbers in the series and store it */
                f[i] = f[i - 1] + f[i - 2];
            }
        }
  
        return f[n];
    }
}
